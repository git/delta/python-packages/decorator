Distutils2==1.0a4
M2Crypto
Pygments==1.6
defusedxml==0.4.1
docutils==0.10
itsdangerous==0.21
passlib==1.6.1
psycopg2==2.5
py-bcrypt==0.3
python-openid==2.2.5
pytz==2013b
raven==3.3.7
redis==2.7.6
requests==1.2.3
six==1.3.0
transaction==1.4.1
zope.browser==2.0.2
zope.component==4.1.0
zope.configuration==4.0.2
zope.contenttype==4.0.1
zope.event==4.0.2
zope.exceptions==4.0.6
zope.i18n==4.0.0a4
zope.i18nmessageid==4.0.2
zope.interface==4.0.5
zope.location==4.0.2
zope.pagetemplate==4.0.4
zope.proxy==4.1.3
zope.publisher==4.0.0a4
zope.schema==4.3.2
zope.security==4.0.0b1
zope.tal==4.0.0a1
zope.tales==4.0.1
zope.traversing==4.0.0a3
